# !/bin/bash
# PROXY_DIR="../volumes/"
# HTTP_TEMPLATE="templates/proxy_http.conf"
# HTTPS_TEMPLATE="templates/proxy_https.conf"
# NGINX_VHOST_FILE="conf.d/02-vhosts.conf"

echo "Running for $1"
TARGET=`basename "$1"`
NAMES=`$(dirname $0)/parsevhosts.sh "$1"`

if [ ! -z "$NAMES" ]; then
    echo "Adding $TARGET: $NAMES"

    cat $HTTP_TEMPLATE | sed "s/__TARGET__/${TARGET}/g" | sed "s/__NAMES__/${NAMES}/g" >> $NGINX_VHOST_FILE
    cat $HTTPS_TEMPLATE | sed "s/__TARGET__/${TARGET}/g" | sed "s/__NAMES__/${NAMES}/g" >> $NGINX_VHOST_FILE
else
    echo "Nothing for $TARGET"
fi


#find "$PROXY_DIR" -maxdepth 1 -type d -exec process {} \;

#find . -name \*.txt -exec process {} \;
