# !/bin/bash
: ${1?"Usage: $0 PATH"}
# L'option --include="*.conf" n'est pas supportée par Alpine
names=`grep -Eohr "Server(Alias|Name).+" $1/ | sed 's/ServerAlias//g' | sed 's/ServerName//g' | sed 's/localhost//g' | sed 's/^\s//g'`
uniq_names=`echo "$names" | tr ' ' '\n' | sort -u | tr '\n' ' ' | sed 's/^\s//g'`
echo "$uniq_names" | sed -r 's/\*\.[^ ]+//g' | sed -r 's/ +/ /g' | sed 's/\s$//g'
