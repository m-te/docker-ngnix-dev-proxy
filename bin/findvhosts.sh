# !/bin/bash
# PROXY_DIR="../volumes/"
# HTTP_TEMPLATE="templates/proxy_http.conf"
# HTTPS_TEMPLATE="templates/proxy_https.conf"
# NGINX_VHOST_FILE="conf.d/02-vhosts.conf"

echo "# $(date)" > $NGINX_VHOST_FILE
find "$PROXY_DIR" -maxdepth 1 -mindepth 1 -type d -exec bash $(dirname $0)/buildvhosts.sh {} \;

#find . -name \*.txt -exec process {} \;
