FROM nginx:stable-alpine
MAINTAINER Marc Teinturier <marc.teinturier@gmail.com>

ENV PROXY_DIR /opt/proxy
ENV HTTP_TEMPLATE /opt/templates/proxy_http.conf
ENV HTTPS_TEMPLATE /opt/templates/proxy_https.conf
ENV NGINX_VHOST_FILE /etc/nginx/conf.d/02-vhosts.conf

RUN apk update && apk add --update bash && rm -rf /var/cache/apk/*
RUN mkdir -p /opt/proxy
RUN mkdir -p /opt/bin
RUN mkdir -p /opt/templates
RUN rm /etc/nginx/conf.d/default.conf

ADD conf.d/ /etc/nginx/conf.d/
ADD ssl/ /etc/nginx/ssl/
ADD templates/ /opt/templates
ADD bin/ /opt/bin
RUN /bin/chmod +x /opt/bin/*

# Démarrage d'Apache
ENTRYPOINT ["/bin/bash", "/opt/bin/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
